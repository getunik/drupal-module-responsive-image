<?php

namespace Drupal\responsive_image;


class ResponsiveFormatterDataSrc extends \Drupal\responsive_image\ResponsiveFormatterBase
{
	/**
     * Called by responsive_image_field_formatter_settings_form with the same parameters
     */
    function settingsForm($field, $instance, $view_mode, $form, &$form_state)
    {
        $display = $instance['display'][$view_mode];
        $type = $instance['display']['default']['type'];
        $settings = $display['settings'];
        $image_styles = image_style_options(FALSE, PASS_THROUGH);

        $element['responsive_type'] = array(
                    '#title' => t('Responsive Type'),
                    '#type' => 'select',
                    '#default_value' => $settings['responsive_type'],
                    '#options' => array(
                        'multiple' => t('Multiple image styles with data source'),
                    ),
                );

        // The format for the image_style field is "{style}[@{width}]; ..." where style is the machine name
        // of an image style and with is the (optional) breakpoint with (in pixels) after which the is being
        // used. A reasonable configuration could look something like this: "small; medium@400; original@600".
        // In the case of the basic responsive image formatter, just specify the name of the image style here.

        $element['image_style'] = array(
            '#title' => t('Image style settings "imagestyle@attribute;...", eg "md@thumbnail"'),
            '#description' => t('The attribute name will be used as the wildcard in a data-src-* attribute - Styles: @formats;', array('@formats' => implode('; ', array_keys($image_styles)))),
            '#type' => 'textfield',
            '#default_value' => $settings['image_style'],
        );

        $element['image_class'] = array(
            '#title' => t('Image CSS Classes'),
            '#type' => 'textfield',
            '#default_value' => $settings['image_class'],
        );

        $element['image_link'] = array(
            '#title' => t('Link image to'),
            '#type' => 'select',
            '#default_value' => $settings['image_link'],
            '#empty_option' => t('Nothing'),
            '#options' => array(
                'content' => t('Content'),
                'file' => t('File'),
            ),
        );

        return $element;
    }

    /**
     * Actual render function for the 'multiple' style
     *
     * @param stdClass $image_info - the prepared image info object
     * @see BaseResonsiveImage::build
     */
    protected function build_multiple($image_info)
    {
        $this->fetchImageDetails($image_info, -1);

        foreach ($image_info->images as $img)
        {
            $image_info->attributes['data-src-' . $img->breakpoint] = $img->uri;
        }

        return  $this->buildRenderArray($image_info);
    }
}
