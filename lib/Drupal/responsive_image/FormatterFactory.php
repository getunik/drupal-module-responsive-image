<?php

namespace Drupal\responsive_image;

class FormatterFactory
{
    /**
     * Instantiates the appropriate responsive image formatter implementation based on the given $type
     *
     * @param $type - responsive image formatter type
     */
    static function create($type)
    {
        switch($type) {
            case "responsive_image" :
                return new ResponsiveFormatterStandard();
            break;

            case "responsive_image_data_src" :
                return new ResponsiveFormatterDataSrc();
            break;

            default:
                throw new \Exception("Invalid responsive image type given: $type");
        }

    }

}
