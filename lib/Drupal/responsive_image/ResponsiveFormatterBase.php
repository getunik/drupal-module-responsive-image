<?php

namespace Drupal\responsive_image;

abstract class ResponsiveFormatterBase
{

    /**
     * Called by responsive_image_field_formatter_view to do the actual theming of the field formatter
     *
     * @param $entity_type
     *   The entity type of the entity that is hosting the field formatter.
     * @param $entity
     *   The entity for which the field formatter should be rendered
     * @param $field
     *   The field definition the formatter is attached to
     * @param $instance
     *   The field instance the formatter is attached to
     * @param $langcode
     *   The language the field is rendered with
     * @param $items
     *   The current field values (primary input for the responsive image formatter)
     * @param $display
     *   The formatters current display configuration
     */
    public function view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
    {
        $settings = $display['settings'];
        $result = array();

        // Check if the formatter involves a link.
        if ($settings['image_link'] == 'content')
        {
            $uri = entity_uri($entity_type, $entity);
        }
        elseif ($settings['image_link'] == 'file')
        {
            $link_file = TRUE;
        }

        foreach ($items as $delta => $item)
        {
            if (isset($link_file))
            {
                $uri = array(
                    'path' => file_create_url($item['uri']),
                    'options' => array(),
                );
            }
            else {
                $uri = NULL;
            }

            $result[] = $this->build($settings, $item, $uri);
        }

        return $result;
    }

    /**
     * Preprocesses the images and calls the underlying renderer type theming implementation (see derived classes)
     *
     * @param $settings
     *   The settings of the formatter in the current view mode
     * @param $image
     *   The Drupal image file
     * @param $target
     *   The link target or NULL if the image should not be linked to anything. If not NULL, an array('path' => 'the/path', 'options' => array(...)) is expected
     *
     * @ingroup themeable
     */
    public function build($settings, $image, $target)
    {
        $image_info = new \stdClass();
        $image_info->file = $image;
        $image_info->target = isset($target) ? $target : NULL;
        $image_info->style = array(
            'type' => $settings['responsive_type'],
            'class' => explode(' ', $settings['image_class']),
        );
        $image_info->image_style = $settings['image_style'];
        $image_info->uri = file_create_url($image_info->file['uri']);
        $image_info->attributes = array();

        if (empty($image_info->file))
        {
            return '';
        }

        // The 'alt' and 'title' attributes that can be found in $image_info->file are unfortunately not
        // properly translated due to some file entity caching. To work around this issue, we extract the
        // alt and title attribute values directly from the corresponding fields as defined in the
        // file_entity_alt and file_entity_title variables.
        //
        // @see file_entity_file_formatter_info()
        // @see file_entity_file_formatter_file_image_view()
        $file_obj = file_load($image_info->file['fid']);
        $replace_options = array(
            'clear' => TRUE,
            'sanitize' => FALSE,
        );
        $alt_expression = variable_get('file_entity_alt', '[file:field_file_image_alt_text]');
        $title_expression = variable_get('file_entity_title', '[file:field_file_image_title_text]');

        $image_info->attributes['alt'] = token_replace($alt_expression, array('file' => $file_obj), $replace_options);
        $image_info->attributes['title'] = token_replace($title_expression, array('file' => $file_obj), $replace_options);

        if (isset($image_info->file['attributes']))
        {
            $image_info->attributes = array_merge($image_info->file['attributes'], $image_info->attributes);
        }

        array_unshift($image_info->style['class'], 'responsive-' . $image_info->style['type']);
        $image_info->attributes['class'] = implode(' ', $image_info->style['class']);

        $renderer = 'build_' . $image_info->style['type'];

        return $this->{$renderer}($image_info); //don't forget to implement the specific renderer in the child class
    }

    /**
     * Called by responsive_image_field_formatter_settings_summary with the same parameters
     */
    public function settingsSummary($field, $instance, $view_mode)
    {
        $display = $instance['display'][$view_mode];
        $settings = $display['settings'];

        $image_styles = image_style_options(FALSE, PASS_THROUGH);
        // Unset possible 'No defined styles' option.
        unset($image_styles['']);

        $summary = array();

        if (isset($settings['responsive_type']))
        {
            $summary[] = t('Responsive: @type', array('@type' => $settings['responsive_type']));
        }

        $link_types = array(
            'content' => t('Linked to content'),
            'file' => t('Linked to file'),
        );
        // Display this setting only if image is linked.
        if (isset($link_types[$settings['image_link']]))
        {
            $summary[] = $link_types[$settings['image_link']];
        }

        return implode('<br />', $summary);
    }

    /**
     * Helper function to fill the $image_info with an array of image definitions
     * as determined through the styles configuration.
     *
     * @param stdClass $image_info - image info object
     * @param int $max (optional) - the maximum number of image definitions that should
     *        be generated; generates all definitions found in the config string by default.
     */
    public function fetchImageDetails($image_info, $max = -1)
    {
        $image_info->images = array();
        $definitions = explode(';', $image_info->image_style);

        if (count($definitions) === 1 && trim($definitions[0]) === '')
        {
            $definitions = array('original');
        }

        $max = ($max < 0 ? count($definitions) : min(count($definitions), $max));

        for ($i = 0; $i < $max; $i++)
        {
            $img = new \stdClass();
            $parts = explode('@', $definitions[$i]);
            $style_name = trim($parts[0]);
            $breakpoint = count($parts) > 1 ? trim($parts[1]) : NULL;

            $dimensions = array(
                'width' => isset($image_info->file['width']) ? $image_info->file['width'] : 0,
                'height' => isset($image_info->file['height']) ? $image_info->file['height'] : 0,
            );

            if ($style_name === 'original')
            {
                $img->uri = file_create_url($image_info->file['uri']);
            }
            else
            {
                image_style_transform_dimensions($style_name, $dimensions);
                $img->uri = image_style_url($style_name, $image_info->file['uri']);
            }

            $img->width = $dimensions['width'];
            $img->height = $dimensions['height'];
            $img->breakpoint = $breakpoint;

            $image_info->images[] = $img;
        }

        module_invoke_all('responsive_image_preprocess', $image_info);
    }

    /**
     * Wraps the given image into a link to the image info's target if that
     * is specified.
     *
     * @param stdClass $image_info - image info object
     * @param $image_size - flag indicating whether to add image widht/height
     */
    protected function buildRenderArray($image_info, $image_size = false)
    {
        // theme_image() requires the 'alt' attributes passed as its own variable.
        // @see http://drupal.org/node/999338
        $image = array(
            '#theme' => 'image',
            '#attributes' => $image_info->attributes,
            '#path' => $image_info->images[0]->uri,
            '#title' =>  $image_info->attributes['title'],
            '#alt' =>  $image_info->attributes['alt'],
        );

        if ($image_size === true)
        {
            $image = array_merge($image, array('#width' => $image_info->images[0]->width, '#height' => $image_info->images[0]->height));
        }

        $link = array(
            '#tree' => TRUE,
        );

        if ($image_info->target !== NULL && isset($image_info->target['path']))
        {
            $url = url($image_info->target['path']);
            $link['#prefix'] = '<a href="' . $url . '">';
            $link['#suffix'] = '</a>';
        }

        $link['image'] = $image;

        return array($link);
    }

}
